FROM amazoncorretto:8
MAINTAINER Jeffrey Bird <jeff@jbird.org>

RUN yum -y install unzip && yum clean all && rm -rf /var/cache/yum

ENV LUCEE_VERSION=5.3.2.77
ENV ORACLE_VERSION 12.1.0.2

WORKDIR /app/lucee
RUN curl -Lo lucee.zip https://release.lucee.org/rest/update/provider/express/$LUCEE_VERSION && \
    unzip lucee.zip && rm -f lucee.zip \
    && chmod +x /app/lucee/bin/*.sh

WORKDIR /app/lucee/lucee-server/deploy
RUN curl -Lo ojdbc7-$ORACLE_VERSION.lex \
    "https://extension.lucee.org/rest/extension/provider/full/D4EDFDBD-A9A3-E9AF-597322D767E0C949?version=$ORACLE_VERSION"

WORKDIR /app/lucee

EXPOSE 8888 8009

CMD /app/lucee/bin/catalina.sh run
